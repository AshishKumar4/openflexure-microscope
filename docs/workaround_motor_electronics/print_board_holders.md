# Print the holders for the motor control electronics

This page describes the parts to print if you are using three separate motor driver boards and an Arduino Nano microcontroller to drive your motors.

{{BOM}}

[PLA filament]: ../parts/materials/pla_filament.md "{cat:material}"
[RepRap-style printer]: ../parts/tools/rep-rap.md "{cat:tool}"

## Print the converter plate and board gripper {pagestep}

* Using a [RepRap-style printer]{qty:1}, print the [nano_converter_plate.stl](../models/nano_converter_plate.stl){previewpage} and [nano_converter_plate_gripper.stl](../models/nano_converter_plate_gripper.stl){previewpage} using [PLA filament]{qty: 50 grams}.

[nano converter plate]{output, qty:1, hidden}
[nano converter plate gripper]{output, qty:1, hidden}

